-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Фев 27 2021 г., 22:32
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571oga`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Билет`
--

CREATE TABLE `Билет` (
  `ID` int NOT NULL COMMENT 'ID билета',
  `ID_сеанса` int NOT NULL,
  `ID_места` int NOT NULL,
  `ФИО` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Билет`
--

INSERT INTO `Билет` (`ID`, `ID_сеанса`, `ID_места`, `ФИО`) VALUES
(1, 2, 1, 'Яшкин М.А.'),
(2, 2, 1, 'Петров А.М.'),
(3, 3, 12, 'Мухин А.П.'),
(4, 3, 9, 'Обломов Е.Р.');

-- --------------------------------------------------------

--
-- Структура таблицы `Зал`
--

CREATE TABLE `Зал` (
  `ID` int NOT NULL COMMENT 'ID зала',
  `Наименование` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Зал`
--

INSERT INTO `Зал` (`ID`, `Наименование`) VALUES
(3, '3 Relax'),
(4, '4 Jolly'),
(5, '5 IMAX');

-- --------------------------------------------------------

--
-- Структура таблицы `Место`
--

CREATE TABLE `Место` (
  `ID` int NOT NULL COMMENT 'ID места',
  `ID_зала` int NOT NULL,
  `Ряд` int NOT NULL,
  `Номер` int NOT NULL COMMENT 'Номер места',
  `Ценовая_категория` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Место`
--

INSERT INTO `Место` (`ID`, `ID_зала`, `Ряд`, `Номер`, `Ценовая_категория`) VALUES
(1, 4, 1, 1, 'Первая'),
(2, 4, 1, 2, 'Первая'),
(3, 4, 2, 1, 'Вторая'),
(4, 4, 2, 2, 'Вторая'),
(5, 4, 3, 1, 'Третья '),
(6, 4, 3, 2, 'Третья'),
(7, 3, 1, 1, 'Первая'),
(8, 3, 1, 2, 'Первая'),
(9, 3, 2, 1, 'Вторая'),
(10, 3, 2, 2, 'Вторая'),
(11, 3, 3, 1, 'Третья'),
(12, 3, 3, 2, 'Третья'),
(32, 5, 1, 1, 'Первая'),
(33, 5, 1, 2, 'Первая'),
(34, 5, 1, 3, 'Первая'),
(35, 5, 1, 4, 'Первая'),
(36, 5, 2, 1, 'Вторая'),
(37, 5, 2, 2, 'Вторая'),
(38, 5, 2, 3, 'Вторая'),
(39, 5, 2, 4, 'Вторая'),
(40, 5, 3, 1, 'Третья'),
(41, 5, 3, 2, 'Третья'),
(42, 5, 3, 3, 'Третья'),
(43, 5, 3, 4, 'Третья');

-- --------------------------------------------------------

--
-- Структура таблицы `Сеанс`
--

CREATE TABLE `Сеанс` (
  `ID` int NOT NULL COMMENT 'ID сеанса',
  `ID_зала` int NOT NULL,
  `ID_фильма` int NOT NULL,
  `Дата_и_время_начала` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Сеанс`
--

INSERT INTO `Сеанс` (`ID`, `ID_зала`, `ID_фильма`, `Дата_и_время_начала`) VALUES
(2, 4, 1, '2021-02-28 10:20:00'),
(3, 3, 4, '2021-02-28 16:00:00'),
(4, 5, 3, '2021-02-28 17:30:00'),
(6, 3, 5, '2021-02-27 19:15:00');

-- --------------------------------------------------------

--
-- Структура таблицы `Фильм`
--

CREATE TABLE `Фильм` (
  `ID` int NOT NULL COMMENT 'ID фильма',
  `Наименование` varchar(255) NOT NULL,
  `Продолжительность` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Фильм`
--

INSERT INTO `Фильм` (`ID`, `Наименование`, `Продолжительность`) VALUES
(1, 'В поисках немо', '01:41:00'),
(2, 'Джентельмены', '01:53:00'),
(3, 'Мстители', '02:17:00'),
(4, 'Зеленая миля', '03:09:00'),
(5, 'Интерстеллар', '02:49:00');

-- --------------------------------------------------------

--
-- Структура таблицы `Цена`
--

CREATE TABLE `Цена` (
  `ID` int NOT NULL COMMENT 'ID цены',
  `ID_сеанса` int NOT NULL,
  `Ценовая_категория` varchar(255) NOT NULL,
  `Цена` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Цена`
--

INSERT INTO `Цена` (`ID`, `ID_сеанса`, `Ценовая_категория`, `Цена`) VALUES
(1, 2, 'Первая', 400),
(2, 2, 'Вторая', 300),
(3, 2, 'Третья', 250),
(4, 3, 'Первая', 390),
(5, 3, 'Вторая', 300),
(6, 3, 'Третья', 240),
(7, 4, 'Первая', 450),
(8, 4, 'Вторая', 390),
(9, 4, 'Третья', 250),
(10, 6, 'Первая', 350),
(11, 6, 'Вторая', 300),
(12, 6, 'Третья', 270);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Билет`
--
ALTER TABLE `Билет`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_сеанса` (`ID_сеанса`),
  ADD KEY `ID_места` (`ID_места`);

--
-- Индексы таблицы `Зал`
--
ALTER TABLE `Зал`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `Место`
--
ALTER TABLE `Место`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_зала` (`ID_зала`);

--
-- Индексы таблицы `Сеанс`
--
ALTER TABLE `Сеанс`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_зала` (`ID_зала`),
  ADD KEY `ID_фильма` (`ID_фильма`);

--
-- Индексы таблицы `Фильм`
--
ALTER TABLE `Фильм`
  ADD PRIMARY KEY (`ID`);

--
-- Индексы таблицы `Цена`
--
ALTER TABLE `Цена`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_сеанса` (`ID_сеанса`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Билет`
--
ALTER TABLE `Билет`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID билета', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `Зал`
--
ALTER TABLE `Зал`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID зала', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `Место`
--
ALTER TABLE `Место`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID места', AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT для таблицы `Сеанс`
--
ALTER TABLE `Сеанс`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID сеанса', AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `Фильм`
--
ALTER TABLE `Фильм`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID фильма', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `Цена`
--
ALTER TABLE `Цена`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT COMMENT 'ID цены', AUTO_INCREMENT=13;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `Билет`
--
ALTER TABLE `Билет`
  ADD CONSTRAINT `Билет_ibfk_1` FOREIGN KEY (`ID_сеанса`) REFERENCES `Сеанс` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Билет_ibfk_2` FOREIGN KEY (`ID_места`) REFERENCES `Место` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Место`
--
ALTER TABLE `Место`
  ADD CONSTRAINT `Место_ibfk_1` FOREIGN KEY (`ID_зала`) REFERENCES `Зал` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Сеанс`
--
ALTER TABLE `Сеанс`
  ADD CONSTRAINT `Сеанс_ibfk_1` FOREIGN KEY (`ID_зала`) REFERENCES `Зал` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `Сеанс_ibfk_2` FOREIGN KEY (`ID_фильма`) REFERENCES `Фильм` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `Цена`
--
ALTER TABLE `Цена`
  ADD CONSTRAINT `Цена_ibfk_1` FOREIGN KEY (`ID_сеанса`) REFERENCES `Сеанс` (`ID`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
