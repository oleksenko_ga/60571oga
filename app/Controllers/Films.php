<?php namespace App\Controllers;

use App\Models\FilmModel;
use Aws\S3\S3Client;

class Films extends BaseController
{
    public function index() //Отображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FilmModel();
        $data ['arr_films'] = $model->getFilm();
        echo view('films/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new FilmModel();
        $data ['arr_films'] = $model->getFilm($id);
        echo view('films/view', $this->withIon($data));
    }

    public function create()
    {

        if (!$this->ionAuth->isAdmin())
        {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('films/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'Наименование' => 'required|min_length[3]|max_length[255]',
                'Продолжительность'  => 'required',
                'picture_url'  => 'is_image[picture_url]',
            ]))
        {

            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture_url');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }


            $model = new FilmModel();
            $data = [
                'Наименование' => $this->request->getPost('Наименование'),
                'Продолжительность' => $this->request->getPost('Продолжительность'),
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Фильм добавлен'));
            return redirect()->to('/films');
        }
        else
        {
            return redirect()->to('/films/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->isAdmin())
        {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
        $model = new FilmModel();

        helper(['form']);
        $data ['arr_films'] = $model->getFilm($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('films/edit', $this->withIon($data));

    }

    public function update()
    {
        helper(['form','url']);
        echo '/films/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'Наименование' => 'required|min_length[3]|max_length[255]',
                'Продолжительность'  => 'required',
            ]))
        {
            $model = new FilmModel();
            $data = [
                'Наименование' => $this->request->getPost('Наименование'),
                'Продолжительность' => $this->request->getPost('Продолжительность'),
            ];
            $model->update($this->request->getPost('id'),$data);
            session()->setFlashdata('message', lang('Успешно изменено'));

            return redirect()->to('/films');
        }
        else
        {
            return redirect()->to('/films/edit/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->isAdmin())
        {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
        $model = new FilmModel();
        $model->delete($id);
        session()->setFlashdata('message', lang('Успешно удалено'));
        return redirect()->to('/films');
    }
}