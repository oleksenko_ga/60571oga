<?php namespace App\Models;
use CodeIgniter\Model;
class FilmModel extends Model
{
    protected $table = 'Фильм'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['Наименование', 'Продолжительность','picture_url'];
    public function getFilm($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

}

