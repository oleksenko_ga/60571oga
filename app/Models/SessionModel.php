<?php
namespace App\Models;
use CodeIgniter\Model;

class SessionModel extends Model
{
    protected $table = 'Сеанс';
    public function getSessionWithFilms($id = null, $search = '')
    {
        $builder = $this->select('*')->join('Фильм','Сеанс.ID_фильма = Фильм.ID')->join('Зал','Сеанс.ID_зала = Зал.ID')->
        like('Наименование', $search,'both', null, true)->
        orlike('Дата_и_время_начала',$search,'both',null,true);
        if (!is_null($id))
        {
            return $builder->where(['Фильм.ID' => $id])->first();
        }
        return $builder;
    }
}