<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="jumbotron text-center">

        <img class="mb-4" src="https://www.flaticon.com/svg/vstatic/svg/704/704845.svg?token=exp=1614878902~hmac=cf18751a89847dd8e4c04a01283126de" alt="" width="72" height="72"><h1 class="display-4">CinemaSky</h1>
        <p class="lead">Это приложение поможет купить билет в кинотеатре</p>
        <a class="btn btn-primary btn-lg" href="auth/login" role="button">Войти</a>
    </div>
<?= $this->endSection() ?>

