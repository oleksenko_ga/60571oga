<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('films/update'); ?>
        <input type="hidden" name="id" value="<?= $arr_films["id"] ?>">

        <div class="form-group">
            <label for="name">Название фильма</label>
            <input type="text" class="form-control <?= ($validation->hasError('Наименование')) ? 'is-invalid' : ''; ?>" name="Наименование"
                   value="<?= $arr_films["Наименование"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Наименование') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="date">Дата рождения</label>
            <input type="time" class="form-control <?= ($validation->hasError('Продолжительность')) ? 'is-invalid' : ''; ?>" name="Продолжительность" value="<?= $arr_films["Продолжительность"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Продолжительность') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>