<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($arr_films)) : ?>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                            <?php if ($arr_films['id'] == 1) : ?>
                                <img height="150" src="https://avatars.mds.yandex.net/get-kinopoisk-image/1599028/58e6b27d-a443-4ca7-a90f-a6fa968dfe25/300x450"  alt="<?= esc($arr_films['Наименование']); ?>">
                            <?php elseif ($arr_films['id'] == 2) : ?>
                                <img height="150" src="https://avatars.mds.yandex.net/get-kinopoisk-image/1599028/637271d5-61b4-4e46-ac83-6d07494c7645/300x450"  alt="<?= esc($arr_films['Наименование']); ?>">
                            <?php elseif ($arr_films['id'] == 3) : ?>
                                <img height="150" src="https://avatars.mds.yandex.net/get-kinopoisk-image/1600647/afab999b-c6bb-4fac-a951-03f72fd2b8cf/300x450" alt="<?= esc($arr_films['Наименование']); ?>">
                            <?php elseif ($arr_films['id'] == 4) : ?>
                                <img height="150" src="https://avatars.mds.yandex.net/get-kinopoisk-image/1599028/4057c4b8-8208-4a04-b169-26b0661453e3/300x450"  alt="<?= esc($arr_films['Наименование']); ?>">
                            <?php elseif ($arr_films['id'] == 5) : ?>
                                <img height="150" src="https://avatars.mds.yandex.net/get-kinopoisk-image/1600647/430042eb-ee69-4818-aed0-a312400a26bf/300x450"  alt="<?= esc($arr_films['Наименование']); ?>">
                            <?php else:?>
                                <img height="150" src="<?= esc($arr_films['picture_url']); ?>" class="card-img" alt="<?= esc($arr_films['Наименование']); ?>">
                            <?php endif ?>

                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($arr_films['Наименование']); ?></h5>
                            <p class="card-text"><?= esc($arr_films['Продолжительность']); ?></p>
                        </div>
                        <div>
                            <a href="<?= base_url()?>/films/edit/<?= esc($arr_films['id']); ?>" class="btn btn-primary" style="margin-bottom:15px;">Изменить</a>
                            <a href="<?= base_url()?>/films/delete/<?= esc($arr_films['id']); ?>" class="btn btn-danger" style="margin-bottom:15px;">Удалить</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <p>Фильм не найден.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>