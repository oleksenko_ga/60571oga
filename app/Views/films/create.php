<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('Films/store'); ?>
        <div class="form-group">
            <label for="name">Название фильма</label>
            <input type="text" class="form-control <?= ($validation->hasError('Наименование')) ? 'is-invalid' : ''; ?>" name="Наименование"
                   value="<?= old('Наименование'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Наименование') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="date">Продолжительность</label>
            <input type="time" class="form-control <?= ($validation->hasError('Продолжительность')) ? 'is-invalid' : ''; ?>" name="Продолжительность" value="<?= old('Продолжительность'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('Продолжительность') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="name">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>" name="picture_url">
            <div class="invalid-feedback">
                <?= $validation->getError('picture_url') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Создать</button>
        </div>
        </form>


    </div>
<?= $this->endSection() ?>