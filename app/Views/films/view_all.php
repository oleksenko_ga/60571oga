<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Фильмы</h2>

        <?php if (!empty($arr_films) && is_array($arr_films)) : ?>
            <?php foreach ($arr_films as $item): ?>

                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if ($item['id'] == 1) : ?>
                                <img height="150" src="https://avatars.mds.yandex.net/get-kinopoisk-image/1599028/58e6b27d-a443-4ca7-a90f-a6fa968dfe25/300x450"  alt="<?= esc($item['Наименование']); ?>">
                            <?php elseif ($item['id'] == 2) : ?>
                                <img height="150" src="https://avatars.mds.yandex.net/get-kinopoisk-image/1599028/637271d5-61b4-4e46-ac83-6d07494c7645/300x450" alt="<?= esc($item['Наименование']); ?>">
                            <?php elseif ($item['id'] == 3) : ?>
                                <img height="150" src="https://avatars.mds.yandex.net/get-kinopoisk-image/1600647/afab999b-c6bb-4fac-a951-03f72fd2b8cf/300x450" alt="<?= esc($item['Наименование']); ?>">
                            <?php elseif ($item['id'] == 4) : ?>
                                <img height="150" src="https://avatars.mds.yandex.net/get-kinopoisk-image/1599028/4057c4b8-8208-4a04-b169-26b0661453e3/300x450" alt="<?= esc($item['Наименование']); ?>">
                            <?php elseif ($item['id'] == 5) : ?>
                                <img height="150" src="https://avatars.mds.yandex.net/get-kinopoisk-image/1600647/430042eb-ee69-4818-aed0-a312400a26bf/300x450"  alt="<?= esc($item['Наименование']); ?>">
                            <?php else:?>
                            <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['Наименование']); ?>">
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= esc($item['Наименование']); ?></h5>
                                <p class="card-text"><?= esc($item['Продолжительность']); ?></p>
                                <a href="<?= base_url()?>/Films/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>
                            </div>
                        </div>
                    </div>

                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти фильмы</p>
        <?php endif ?>
        <a href="<?= base_url()?>/films/store" class="btn btn-primary">Добавить фильм</a>
    </div>
<?= $this->endSection() ?>