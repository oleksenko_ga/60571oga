<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Cinema extends Migration
{
    public function up()
    {
        if (!$this->db->tableexists('Фильм'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'Наименование' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'Продолжительность' => array('type' => 'TIME', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('Фильм', TRUE);
        }

        if (!$this->db->tableexists('Зал'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'Наименование_зала' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('Зал', TRUE);
        }

        if (!$this->db->tableexists('Место'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_зала' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'Ряд' => array('type' => 'INT', 'null' => FALSE),
                'Номер' => array('type' => 'INT', 'null' => FALSE),
                'Ценовая_категория' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('ID_зала','Зал','ID','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('Место', TRUE);
        }



        if (!$this->db->tableexists('Сеанс'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_зала' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'ID_фильма' => array('type' => 'INT','unsigned' => TRUE, 'null' => FALSE),
                'Дата_и_время_начала' => array('type' => 'DATETIME', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('ID_зала','Зал','ID','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('ID_фильма','Фильм','ID','RESTRICT','RESTRICT');

            // create table
            $this->forge->createtable('Сеанс', TRUE);
        }





        // activity_type
        if (!$this->db->tableexists('Билет'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_сеанса' => array('type' => 'INT','unsigned' => TRUE, 'null' => FALSE),
                'ID_места' => array('type' => 'INT','unsigned' => TRUE, 'null' => FALSE),
                'ФИО' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('ID_сеанса','Сеанс','ID','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('ID_места','Место','ID','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('Билет', TRUE);
        }






        if (!$this->db->tableexists('Цена'))
        {
            // Setup Keys
            $this->forge->addkey('ID', TRUE);

            $this->forge->addfield(array(
                'ID' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_сеанса' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'Ценовая_категория' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'Цена' => array('type' => 'INT', 'null' => FALSE),
            ));
            $this->forge->addForeignKey('ID_сеанса','Сеанс','ID','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('Цена', TRUE);
        }

    }

    //--------------------------------------------------------------------

    public function down()
    {

        $this->forge->droptable('Билет');
        $this->forge->droptable('Зал');
        $this->forge->droptable('Место');
        $this->forge->droptable('Сеанс');
        $this->forge->droptable('Фильм');
        $this->forge->droptable('Цена');
    }
}