<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addpictureurlfield extends Migration
{
    public function up()
    {
        if ($this->db->tableexists('Фильм'))
        {
            $this->forge->addColumn('Фильм',array(
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
    }
    public function down()
    {
        $this->forge->dropColumn('Фильм', 'picture_url');
    }
}